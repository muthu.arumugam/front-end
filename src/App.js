import "./App.css";
import Home from "./pages/Home";
import { Route, Switch, BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <>
      <BrowserRouter>
            <Switch>
            <Route exact path="/" component={Home} />
            </Switch>
            </BrowserRouter>
    </>
  );
}

export default App;
