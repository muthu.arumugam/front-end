import React, { Component } from 'react'
import "../styles/footer.scss";

export default class Footer extends Component {
    render() {
        return (
            <div className="footer sections">
                <div className="footer-inner">
                    <div className="row content-section">
                        <div class="col-md-4 mb-4 mb-lg-0">
                            <ul class="list-inline mt-4">
                                <li class="list-inline-item"><a href="https://www.facebook.com/develearntechnologies"><img className="logo-img" src="facebook.png" /></a></li>
                                <li class="list-inline-item"><a href="https://www.linkedin.com/company/develearn-technologies"><img className="logo-img" src="linkedin.png" /></a></li>
                                {/*<li class="list-inline-item"><a href=""><img className="logo-img" src="twitter.png" /></a></li>*/}
                                <li class="list-inline-item"><a href="https://mail.google.com/mail/?view=cm&fs=1&to=contact@develearn.in&su=SUBJECT&body=BODY&bcc=contact@develearn.in"><img className="logo-img" src="mail.png" /></a></li>
                                <li class="list-inline-item"><a href="https://www.instagram.com/develearn.institute/"><img className="logo-img" src="instagram.png" /></a></li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <h4>Toll-Free: 1800-210-2989</h4>
                            <h4>Address : 472/22, Mohan Nivas, 1st Floor, Opposite Maheshwari Udyan,
                                Matunga (C.R.), Mumbai, Maharashtra - 400019</h4>
                        </div>
                    </div>
                  
                </div>
                <div className="footer-copyright">
                    <p className="text-foot">All Rights Reserved by Develearn Technologies Pvt Ltd</p>
                </div>
            </div>
        )
    }
}
