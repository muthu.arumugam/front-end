import React from "react";
import "../styles/header.scss";
import LogoText from "../assets/logo.png";
import LogoImg from "../assets/imglogo.png";

function Header() {
  return (
    <>
      <div className="container-fluid header">
        <div className="row py-2 d-flex align-items-center">
          <div className="col-sm-6 logo text-left ">
            <img className="img fluid" src={LogoText} alt="logo img" />
          </div>
          <div className="col-sm-6 logo text-right ">
            <img className="img fluid" src={LogoImg} alt="logo img" />
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;
