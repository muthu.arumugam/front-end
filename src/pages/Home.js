import React from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import "../styles/home.scss";
import ComputerImg from "../assets/computer.svg";
import topleft from "../assets/top-left.svg";
import topright from "../assets/top-right.svg";
import bottomleft from "../assets/bottom-left.svg";
import bottomright from "../assets/bottom-right.svg";
import axios from 'axios';
import { ip } from './constant.js'

import Fade from '../../node_modules/react-reveal/Fade';

export default class Home extends React.Component {
  state = {
    name: '',
    mobileno: '',
    email: '',
    city: '',
  
    sent: false
}

handleName = (e) => {
  this.setState({
      name: e.target.value
  })
}

handleMobile = (e) => {
  this.setState({
      mobileno: e.target.value
  })
}

handleEmail = (e) => {
  this.setState({
      email: e.target.value
  })
}

handleCity = (e) => {
  this.setState({
      city: e.target.value
  })
}

formSubmit = (e) => {

  e.preventDefault();
  
  let data = {
      name: this.state.name,
      mobileno: this.state.mobileno,
      email: this.state.email,
      city: this.state.city  
  }
 
  axios.post(`${ip}/api/form`, data)
      .then(res => {
          this.setState({
              sent: true,
              
          }, this.resetForm())
      }).catch(() => {
          console.log("not sent")
      })

      alert("Successful");
}

resetForm = () => {
  this.setState({
      name: '',
      mobileno: '',
      email: '',
      city: '',
    
  })
 
}

render() {

  return (
    <>
      <div className="showcase">
        <Header />
        
        <section className="page-section" id="contact">
          <div className="container-fluid mt-5 pb-5">
            <div className="row">
              <div className="col-sm-5 cd d-flex justify-content-around">
                <div className="col-sm-11 p-5 formwrapper bg-white border">
                  <form className="w-100"
                    name="form"
                    method="post"
                    action="/"
                   onSubmit={this.formSubmit}
                  >
                    <h2 className="pt-1 font-weight-bold form-title text-center">
                      Let's Connect
                    </h2>
                    <p className="text-dark">
                      Fill in the form and our representatives will get in touch
                      to ensure your compatibility for this course
                    </p>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="name"
                        placeholder="Full Name"
                        value={this.state.name}
                         onChange={this.handleName}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="mobileno"
                        placeholder="Mobile Number"
                        value={this.state.mobileno} 
                        onChange={this.handleMobile}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="email"
                        className="form-control"
                        name="email"
                        placeholder="Email ID"
                        value={this.state.email} 
                        onChange={this.handleEmail}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="city"
                        placeholder="City"
                        value={this.state.city}
                        onChange={this.handleCity}
                      />
                    </div>
                    <div className="form-group text-center">
                      <button className="btn bg-pri btn-text font-weight-bold button-in" >
                        Enquire
                      </button>
                    </div>
                  </form>
                </div>
              </div>

              <div className="col-sm-7 container-fluid bg-white right_div">
                  <h1 className="title text-pri">
                    Data Science Diploma Program Certified by Solapur University
                  </h1>
                  <div className="text-center">
                    <img className="img-fluid" src={require("../assets/building.jpg").default} alt=""/>
                  </div>
              </div>
            </div>
          </div>
        </section>

      </div>

      <Fade down>
        <div className="box-wrapper container">
            <div className="col-sm-12">
              <div className="box">
                <h1 className="text-pri title">
                  10 month intensive program for under Rs. 49,000/-
                </h1>
              </div>
              <div className="box">
                <div className="row">
                  <img className="logo-img" src ={require("../assets/save-money.png").default}></img>
                  <h1 className="text-pri title">EMI starts at Rs. 4,500/-</h1>  
                </div>
              </div>
              <div className="box">
                <div className="row">
                  <img className="logo-img" src ={require("../assets/live.png").default}></img>
                  <h1 className="text-pri title shifttext-right">Live sessions and interactions</h1>
                </div>
              </div>
            </div>
        </div>
      </Fade>

      <div style={{ marginTop: "5rem" }} className=" about">
        <div id="about">
          
          <div className="container">
            <Fade up>
              <div className="col-sm-12">
              <h1 className="text-center section-title text-uppercase font-weight-bold">
                Diploma Program Highlights
              </h1>
              </div>
            </Fade>
          </div>

          <div className="container-fluid mt-5 pb-5">
            <div className="row">
              <Fade up>
                <div className="col-sm-5 d-flex align-items-center justify-content-center">
                  <img alt="..." className="img-fluid robimg" src={ComputerImg} />
                </div>

                <div className="col-sm-7 d-flex align-items-center justify-content-center">
                  <div className="ab_right d-flex align-items-center">
                    <ul>
                      <li>
                        A 10 month program in full-fledged training from fresher
                        to Data Science practitioner
                      </li>
                      <li>Foundations of Python Programming</li>
                      <li>
                        Dive into Data Analytics with Numpy, Pandas, MatplotLib
                      </li>
                      <li>Master Machine Learning using Python or R</li>
                      <li>Statistics &amp; Probability theory essential for data science</li>
                      <li>State-of-the-art Machine Learning algorithms</li>
                      <li>Explodatory Data Analytics &amp; Data visualization</li>
                      <li>Capstone project - based experiential learning</li>
                      <li>Become Job ready through an Industry approved curriculum</li>
                    </ul>
                  </div>
                </div>
              </Fade>
            </div>
          </div>

        </div>
      </div>

      <Fade up>
        <div className="yellowdiv mb-5 container">
          <h3 className="text-pri title">
              The Data Science Diploma program equips you for the best jobs
              currently in the market as you will be instantly eligible for
              most  Data Scientist job listings!{" "}
          </h3>
        </div>
      </Fade>

      <section id="feature" className="feature">
        {/*<Reveal effect="fadeInUp">*/}
        <div className="container-fluid">
          <div className="container-fluid">
            <Fade up>
              <h1 className={`text-center mt-0 font-weight-bold`}>
                Why Develearn ?
              </h1>
              <p className="mt-3 font-weight-regular psolid">
                Established by a team of IITians and enthusiastic industry
                specialists from various organisations with the vision of
                providing world class quality education to fill the gap between
                industry and academia. While deliverying a wealth of experience in
                all things pertaining to data, we provide rigorous technical and
                strategic training for highly motivated individual and
                corporations.
              </p>
            </Fade>
          </div>
          <div className="row pt-5">
            <div className="d-flex align-items-center col-sm-6 justify-content-center icon">
              <Fade left>
              < div className="column align-content-center">
                  <img className="icon" src={topleft} alt="" />
                  <h1 className="text-pri ">Practical Learning over Theoretical Knowledge</h1>
                </div>
              </Fade>
            </div>
            <div className="d-flex align-items-center col-sm-6 justify-content-center icon">
              <Fade right>
                <div className="column align-content-center">
                  <img className="icon" src={topright} alt="" />
                  <h1 className="text-pri ">Master Machine Learning using Python & R</h1>
                </div>
                
              </Fade>
            </div>
          </div>
          <div className="row pt-5">
            <div className="d-flex align-items-center col-sm-6 justify-content-center icon">
              <Fade left>
                <div className="column align-content-center">
                  <img className="icon" src={bottomleft} alt="" />
                  <h1 className="text-pri ">Exposure to Real-World Data Science Projects</h1>
                </div>
              </Fade>
            </div>
            <div className="d-flex align-items-center col-sm-6 justify-content-center icon">
              <Fade right>
                <div className="column align-content-center">
                  <img className="icon" src={bottomright} alt="" />
                  <h1 className="text-pri ">Live Online sessions Self-paced Coursework</h1>
                </div>
              </Fade>
            </div>
          </div>
        </div>
       
      </section>

      
      <div className="footer mt-5 mb-5">
        <div className="container">
          <div className="row test">
            <Fade up>
            <div className="col-sm-2 testleft img">
              <img
                src={require("../assets/testimonial.png").default}
                alt=""
              />
            </div>
            <div className="col-sm-10 testright bg-pri">
              <h3 className="text-white ">
                “ The Faculty are very experienced and skillfull in teaching
                these languages by giving real life illustrations and examples.
                Develearn is the best platform to learn and use your creativity
                to code some amazing programs. “
              </h3>
            </div>
            </Fade>
          </div>
        </div>
      </div>

      <Footer/>
    </>
    
  );
}

}
