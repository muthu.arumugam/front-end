import React from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import "../styles/home.scss";
import ComputerImg from "../assets/computer.svg";
import Group13 from "../assets/Group 13.svg";
import Group14 from "../assets/Group 14.svg";
import Group15 from "../assets/Group 15.svg";
import Group16 from "../assets/Group 16.svg";
import axios from 'axios';
import { ip } from './constant.js'
import ResponsiveFontSize from "react-responsive-font-size";

export default class Home extends React.Component {
  state = {
    name: '',
    mobileno: '',
    email: '',
    city: '',
  
    sent: false

}

handleName = (e) => {
  this.setState({
      name: e.target.value
  })
}

handleMobile = (e) => {
  this.setState({
      mobileno: e.target.value
  })
}

handleEmail = (e) => {
  this.setState({
      email: e.target.value
  })
}

handleCity = (e) => {
  this.setState({
      city: e.target.value
  })
}


formSubmit = (e) => {


  e.preventDefault();
  
  let data = {
      name: this.state.name,
      mobileno: this.state.mobileno,
      email: this.state.email,
      city: this.state.city
     
  }
 
  axios.post(`${ip}/api/form`, data)
      .then(res => {
          this.setState({
              sent: true,
              
          }, this.resetForm())
      }).catch(() => {
          console.log("not sent")
      })

      alert("Successful");
}

resetForm = () => {
  this.setState({
      name: '',
      mobileno: '',
      email: '',
      city: '',
    
  })
 
}

render() {

  return (
    <>
      <div className="showcase">
        <Header />
        <section className="page-section" id="contact">
          <div className="container-fluid mt-5 pb-5">
            <div className="row">
              <div className="col-sm-5 cd d-flex justify-content-around">
                <div className="col-sm-10 p-5 formwrapper bg-white border">
                  <form className="w-100"
                    name="form"
                    method="post"
                    action="/"
                   onSubmit={this.formSubmit}
                  >
                    <h2 className="pt-1 font-weight-bold form-title text-center">
                      Let's Connect
                    </h2>
                    <p className="text-dark">
                      Fill in the form and our representatives will get in touch
                      to ensure your compatibility for this course
                    </p>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="name"
                        placeholder="Full Name"
                        value={this.state.name}
                         onChange={this.handleName}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="mobileno"
                        placeholder="Mobile Number"
                        value={this.state.mobileno} 
                        onChange={this.handleMobile}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="email"
                        className="form-control"
                        name="email"
                        placeholder="Email ID"
                        value={this.state.email} 
                        onChange={this.handleEmail}
                      />
                    </div>

                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        name="city"
                        placeholder="City"
                        value={this.state.city}
                        onChange={this.handleCity}
                      />
                    </div>
                    <div className="form-group text-center">
                      <button className="btn bg-pri text-white font-weight-bold button-in" >
                        Enquire
                      </button>
                    </div>
                  </form>
                </div>
              </div>

              <div className="col-sm-7 rs1 bg-white right_div">
                {/*<div className="col-sm-12">*/}
                  <h1 className="title text-pri">
                    Data Science Diploma Program Certified by Solapur University
                  </h1>
                  <div className="text-center">
                    <img className="img-fluid" src={require("../assets/building.jpg").default} alt=""/>
                    {/*<img
                       className="m_img img-fluid"
                       src={require("../assets/building.jpg").default}
                       alt=""
                       />*/}
                  </div>
                {/*</div>*/}
              </div>
            </div>
          </div>
        </section>
      </div>
      {/*<div className="">
        <div className="box-wrapper mb-5 container">
          <div className="row">
            <div className="col-sm-12">
              <div className="box">
                <h1 className="text-pri title">
                  10 month intensive program for under Rs. 49,000/-
                </h1>
              </div>
              <div className="box">
              <div className="row w-100">
                <div className="col-sm-4 text-right">
                <img className="logo-img" src ={require("../assets/save-money.png").default}></img>
                </div>
                <div className="col-sm-8  text-left">
                <h1 className="text-pri title">EMI starts at Rs. 4,500/-</h1>
                </div>
              </div>
              </div>
              <div className="box">
                <div className="row w-100">
                  <div className="col-sm-4 text-right ">
                    <img className="logo-img" src ={require("../assets/live.png").default}></img>
                  </div>
                  <div className="col-sm-8 text-left ">
                    <h1 className="text-pri title"> Live sessions and interactions</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>*/}

      {/*<div style={{height: 400px}}>*/}
      {/*<ResponsiveFontSize ratio={0.5} optionsObject={{ setFontSize: true, globalVariableName: '--my-variable', lockFontSize: false }}>
      </ResponsiveFontSize>*/}
          <div className="box-wrapper mb-5 container">
            <div style={{display: 'flex', lineHeight: '40px', justifyContent:'center', alignItems: 'center'}}>
                <h1 className="text-pri title">10 month intensive program for under Rs. 49,000/-</h1>
            </div>
            <div style={{display: 'flex', lineHeight: '40px', justifyContent:'center', alignItems: 'center'}}> {/*</div>className="box row w-100 col-sm-10 text-right">*/}
              <img className="logo-img" src ={require("../assets/save-money.png").default}></img>
              <h1 className="text-pri title">EMI starts at Rs. 4,500/-</h1>
            </div>
            <div style={{display: 'flex', lineHeight: '40px', justifyContent:'center', alignItems: 'center'}}> 
              <img className="logo-img" src ={require("../assets/live.png").default}></img>
              <h1 className="text-pri title">Live sessions and interactions</h1>
            </div>
          </div>
        
      


      <div style={{ marginTop: "13rem" }} className=" about">
        <div id="about">
          <div className="container">
            <h1 className={`text-center text-pri mt-0 text-uppercase font-weight-bold`}>
              Diploma Program Highlights{" "}
            </h1>
          </div>

          <div className="container-fluid mt-5 pb-5">
            <div className="row">
              <div className="col-sm-6 d-flex align-items-center justify-content-center">
                <img alt="..." className="img-fluid robimg" src={ComputerImg} />
              </div>

              <div className="col-sm-6 rightcon d-flex flex-column align-items-center ">
                <div className="ab_right h-100 d-flex flex-column align-items-center">
                  <ul>
                    <li>
                      A 10 month program in full-fledged training from being a fresher
                      to a Data Science practitioner
                    </li>
                    <li>Foundations of Python Programming</li>
                    <li>
                      Dive into Data Analytics with Nympy, Pandas, MatplotLib
                      i.e. Python for Data Science
                    </li>
                    <li>Master Machine Learning through Python or R</li>
                    <li>Statistics &amp; Probability required for data science</li>
                    <li>State-of-the-art Machine Learning algorithms</li>
                    <li>Explodatory Data Analytics &amp; Data visualization</li>
                    <li>Capstone project - based experiential learning</li>
                    <li>Become Job ready through an Industry approved curriculum</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-5">
        <div className="yellowdiv mb-5 container">
          <div className="row">
            <div className="col-sm-12">
              <div className="">
                <h3 className="text-pri title">
                  The Data Science Diploma program equips you for the best jobs
                  currently in the market as you will be instantly eligible for
                  most  Data Scientist job listings!{" "}
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section id="feature" className="feature">
        <div className="container-fluid">
          <div className="container-fluid">
            <h1 className={`text-center text-pri mt-0 text-uppercase font-weight-bold`}>
              Why Develearn?
            </h1>
            <p className="mt-3 font-weight-bold psolid">
              Established by a team of IITians and enthusiastic industry
              specialists from various organisations with the vision of
              providing world class quality education to fill the gap between
              industry and academia. While deliverying a wealth of experience in
              all things pertaining to data, we provide rigorous technical and
              strategic training for highly motivated individual and
              corporations.
            </p>
          </div>
          <div className="row pt-5">
            <div className="d-flex align-items-center col-sm-6 justify-content-center">
              <div className="style_prevu_kit">
                <img className="icon " src={Group13} alt="" />
              </div>
            </div>
            <div className="d-flex align-items-center col-sm-6 justify-content-center">
              <div className="style_prevu_kit">
                <img className="icon " src={Group14} alt="" />
              </div>
            </div>
          </div>
          <div className="row mb-5 mt-5">
            <div className="d-flex align-items-center col-sm-6 justify-content-center">
              <div className="style_prevu_kit">
                <img className="icon " src={Group16} alt="" />
              </div>
            </div>
            <div className="d-flex align-items-center col-sm-6 justify-content-center">
              <div className="style_prevu_kit">
                <img className="icon " src={Group15} alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="footer mt-5 mb-5">
        <div className="container">
          <div className="row test">
            <div className="col-sm-2">
              <div className=" testleft">
                <img
                  src={require("../assets/testimonial.png").default}
                  alt=""
                />
              </div>
            </div>
            <div className="col-sm-10 testright bg-pri">
              <h3 className="text-white px-5 py-5 ">
                “ The Faculty are very experienced and skillfull in teaching
                these languages by giving real life illustrations and examples.
                Develearn is the best platform to learn and use your creativity
                to code some amazing programs. “
              </h3>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </>
    
  );
}

}
